<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer("bgg_id")->unique();
            $table->string("bgg_slug");
            $table->string("bgg_name")->index();
            $table->decimal("bgg_gbrank", 8, 4)->index();
            $table->decimal("bgg_lcrank", 8, 4);
            $table->decimal("bgg_lcvote", 8, 4);
            $table->string("bgg_type");
            $table->string("bgg_image");
            $table->integer("bgg_year");
            $table->integer("bgg_own");
            $table->decimal("bgg_esw", 8, 4);
            $table->decimal("bgg_esh", 8, 4);
            $table->decimal("bgg_esl", 8, 4);
            $table->decimal("bgg_eswg", 8, 4);
            $table->dateTime("bgg_last_update");


            $table->integer("mnmk_id");
            $table->string("mnmk_name");
            $table->string("mnmk_image");
            $table->integer("mnmk_stock");
            $table->dateTime("mnmk_last_update");
            $table->decimal("mnmk_price");
            $table->decimal("mnmk_retail");
            $table->string("mnmk_url");
            $table->string("mnmk_code");

            $table->decimal("est_price_thb");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
