<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer("user_id")->unsigned()->index();
            $table->integer("game_id")->unsigned()->index();
            $table->integer("post_type")->index();

            $table->boolean("enable_1")->index()->default(0);
            $table->boolean("enable_2")->index()->default(0);

            $table->integer("price_1")->default(0);
            $table->integer("price_2")->default(0);

            $table->text('comment')->nullable();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('game_id')->references('id')->on('games');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
