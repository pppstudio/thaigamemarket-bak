/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('form_post_buy', require('./components/FormPostBuy.vue'));
//Vue.component('tr_game', require('./components/TrGame.vue'));

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue.js!1',
    isOpenBuyForm: false,
    isOpenSaleForm: false,
    BuyFormData: {
      is1Hand: false,
      is2Hand: false

    },

    SaleFormData: {}


  },
  methods: {
    toggleBuyForm: function () {
      //this.isOpenBuyForm = !this.isOpenBuyForm;
      this.isOpenBuyForm = true;
      this.isOpenSaleForm = false;
      return false;
    },
    toggleSaleForm: function () {
      //this.isOpenSaleForm = !this.isOpenSaleForm;
      this.isOpenSaleForm = true;
      this.isOpenBuyForm = false;
      return false;
    },
    closeBuyForm: function () {
      //this.isOpenBuyForm = !this.isOpenBuyForm;
      this.isOpenBuyForm = false;
      return false;
    },
    closeSaleForm: function () {
      //this.isOpenSaleForm = !this.isOpenSaleForm;
      this.isOpenSaleForm = false;
      return false;
    }

  }
});


/**
 * Vue filter to round the decimal to the given place.
 * http://jsfiddle.net/bryan_k/3ova17y9/
 *
 * @param {String} value    The value string.
 * @param {Number} decimals The number of decimal places.
 */
Vue.filter('round', function (value, decimals) {
  if (!value) {
    value = 0;
  }

  if (!decimals) {
    decimals = 0;
  }

  value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
  return value;
});

