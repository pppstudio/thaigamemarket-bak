@extends('layouts.app')
@section('title')
    ราคากลางบอร์ดเกม
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ราคากลางบอร์ดเกม</div>

                    <div class="panel-body">
                        <p> ค้นหาด้วยชื่อ
                        <form action="{{url("/games/search")}}">
                            <div class="input-group col-md-6">
                                <input type="text" class="form-control" placeholder="Search for..." name="keyword"/>
                                  <span class="input-group-btn">
                                    <input class="btn btn-default" type="submit" value="Go!"/>
                                  </span>

                            </div><!-- /input-group -->
                        </form>
                        </p>

                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-default" type="button">Go!</button>--}}
                        {{--</span>--}}
                        {{--</div><!-- /input-group -->--}}

                        <p> กดที่ตัวหนังสือเพื่อค้นหา


                        <ul class="pagination" style="margin: 0">
                            <li><a href="{{url("/games-alphabet/0-9")}}">0-9 </a></li>
                            @foreach (range('a', 'z') as $char)
                                <li><a href="{{url("/games-alphabet/".$char)}}">{{strtoupper($char)}}</a></li>
                            @endforeach
                        </ul>
                        </p>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            @if(count($buy_posts)>0)
                <div class="col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">รายการรับซื้อล่าสุด</div>

                        <div class="panel-body">


                            @foreach($buy_posts as $post)

                                <div class="row">
                                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
                                        <img src="{{str_replace("cf.geekdo-images.com","cf.thaigamemarket.com",$post->game->bgg_image)}}"
                                             class="img-thumbnail"/>

                                    </div>


                                    <div class="col-xs-8 col-md-9 col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-12 h4">

                                                <a href="{{url("/game/".$post->game->bgg_id."/".$post->game->slug)}}">{{$post->game->bgg_name}}</a>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-3 col-lg-3">
                                                <img src="{{$post->user->profile_url}}" class="img-thumbnail"/>
                                            </div>
                                            <div class="col-xs-9 col-lg-9">

                                                <a target="_blank"
                                                                                     href="https://www.facebook.com/{{$post->user->facebook_id}}">{{$post->user->name}}</a>
                                                <br/>
                                                {{$post->created_at->toFormattedDateString()}}
                                                <i>({{$post->created_at->diffForHumans()}})</i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-offset-4 col-xs-2 col-md-offset-8  col-lg-offset-0  col-lg-2 text-left">
                                        @if($post->enable_1)
                                            <h4>
                                                <span class="text-success">มือ 1</span>
                                            </h4>
                                        @endif
                                        @if($post->enable_2)
                                            <h4>
                                                <span class="text-success">มือ 2</span>
                                            </h4>
                                        @endif
                                    </div>
                                    <div class="col-xs-2 col-lg-2 text-right">
                                        @if($post->enable_1)
                                            <h4>
                                                {{$post->price_1>0?" ".number_format($post->price_1)."":"-"}}
                                            </h4>
                                        @endif
                                        @if($post->enable_2)
                                            <h4>
                                                {{$post->price_2>0?" ".number_format($post->price_2)."":"-"}}
                                            </h4>
                                        @endif
                                    </div>

                                    <div class="col-xs-offset-10 col-xs-2 col-lg-2 text-right">

                                        @if (!Auth::guest() && Auth::user()->id==$post->user_id)
                                            <a class="btn btn-danger"
                                               href="{{url("post/".$post->id."/delete")}}">ลบ</a>
                                        @else

                                            <a class="btn btn-primary" target="_blank"
                                               href="https://www.facebook.com/messages/t/{{$post->user->facebook_id}}">ติดต่อ</a>
                                        @endif
                                    </div>

                                </div>
                                <hr/>
                            @endforeach


                        </div>
                    </div>
                </div>


            @endif


            @if(count($sell_posts)>0)

                <div class="col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">รายการขายล่าสุด</div>

                        <div class="panel-body">


                            @foreach($sell_posts as $post)

                                <div class="row">
                                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
                                        <img src="{{str_replace("cf.geekdo-images.com","cf.thaigamemarket.com",$post->game->bgg_image)}}"
                                             class="img-thumbnail"/>

                                    </div>


                                    <div class="col-xs-8 col-md-9 col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-12 h4">

                                                <a href="{{url("/game/".$post->game->bgg_id."/".$post->game->slug)}}">{{$post->game->bgg_name}}</a>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-3 col-lg-3">
                                                <img src="{{$post->user->profile_url}}" class="img-thumbnail"/>
                                            </div>
                                            <div class="col-xs-9 col-lg-9">

                                                <a target="_blank"
                                                   href="https://www.facebook.com/{{$post->user->facebook_id}}">{{$post->user->name}}</a>
                                                <br/>
                                                {{$post->created_at->toFormattedDateString()}}
                                                <i>({{$post->created_at->diffForHumans()}})</i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-offset-4 col-xs-2 col-md-offset-8 col-lg-offset-0 col-lg-2 text-left">
                                        @if($post->enable_1)
                                            <h4>
                                                <span class="text-danger">มือ 1</span>
                                            </h4>
                                        @endif
                                        @if($post->enable_2)
                                            <h4>
                                                <span class="text-danger">มือ 2</span>
                                            </h4>
                                        @endif
                                    </div>
                                    <div class="col-xs-2 col-lg-2 text-right">
                                        @if($post->enable_1)
                                            <h4>
                                                {{$post->price_1>0?" ".number_format($post->price_1)."":"-"}}
                                            </h4>
                                        @endif
                                        @if($post->enable_2)
                                            <h4>
                                                {{$post->price_2>0?" ".number_format($post->price_2)." ":"-ุ"}}
                                            </h4>
                                        @endif
                                    </div>

                                    <div class="col-xs-offset-10 col-xs-2 col-lg-offset-2 col-lg-2 text-right">

                                        @if (!Auth::guest() && Auth::user()->id==$post->user_id)
                                            <a class="btn btn-danger"
                                               href="{{url("post/".$post->id."/delete")}}">ลบ</a>
                                        @else

                                            <a class="btn btn-primary" target="_blank"
                                               href="https://www.facebook.com/messages/t/{{$post->user->facebook_id}}">ติดต่อ</a>
                                        @endif
                                    </div>


                                </div>


                                <div class="row">


                                    <div class="col-lg-12">

                                        @if($post->comment)
                                            <pre class="h5" style="border: none">{{trim($post->comment)}}</pre>
                                        @endif

                                    </div>


                                </div>
                                <div class="row">

                                    @php
                                    $files = [];
                                    $data = json_decode($post->data,true);
                                    if($data && array_key_exists('files',$data)){
                                    $files=$data['files'];
                                    }
                                    @endphp


                                    @foreach($files as $file)
                                        <a href="{{Storage::url($file['path'])}}" target="_blank"
                                           class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                            <img src="{{Storage::url($file['thumbnail'])}}"
                                                 class="img-thumbnail img-responsive"/>
                                        </a>
                                    @endforeach
                                </div>


                                <hr/>
                            @endforeach

                        </div>
                    </div>
                </div>
            @endif


        </div>


    </div>


@endsection
