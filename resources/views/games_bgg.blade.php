@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">รายชื่อเกม</div>

                    <div class="panel-body">
                        <form action="{{url("/games/search")}}">
                            <div class="input-group col-md-6">
                                <input type="text" class="form-control" placeholder="Search for..." name="keyword"
                                       value="{{$keyword}}"/>
                                  <span class="input-group-btn">
                                    <input class="btn btn-default" type="submit" value="Go!"/>
                                  </span>

                            </div><!-- /input-group -->
                        </form>
                        <ul class="pagination">
                            <li class="{{$active=='0-9'?'active':''}}"><a href="{{url("/games-alphabet/0-9")}}">0-9 </a>
                            </li>
                            @foreach (range('a', 'z') as $char)
                                <li class="{{$active==$char?'active':''}}"><a
                                            href="{{url("/games-alphabet/".$char)}}">{{strtoupper($char)}}</a></li>
                            @endforeach
                        </ul>
                        <div class="table-responsive-disable">


                            @if (count($games)>0)


                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                            ชื่อ
                                        </th>
                                        <th class="text-right">
                                            ซื้อ/ขาย
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- @var \App\Game $game --}}
                                    {{--                                {{var_export($games['item'],true)}}--}}
                                    @foreach ($games as $game)
                                        <tr>

                                            <td class="name">
                                                <a class="visible-xs visible-sm big"
                                                   href="{{url("/game/".$game['id']."/".$game['name'])}}">{{$game['name']}}</a>
                                                <a class="visible-md visible-lg"
                                                   href="{{url("/game/".$game['id']."/".$game['name'])}}">{{$game['name']}}</a>

                                                {{--{{var_dump($game['id'])}}--}}
                                                {{--                                            {{var_dump($game['name'])}}--}}

                                            </td>
                                            <td>
                                                <div class="visible-xs visible-sm text-right">
                                                    <a href="{{url("/game/".$game['id']."/".$game['name'])}}"
                                                       class="btn btn-primary btn-lg">เลือก</a>
                                                </div>
                                                <div class="visible-md visible-lg text-right">
                                                    <a href="{{url("/game/".$game['id']."/".$game['name'])}}"
                                                       class="btn btn-primary">เลือก</a>
                                                </div>
                                                {{--                                            {{var_dump($game)}}--}}

                                            </td>
                                        </tr>


                                    @endforeach
                                    </tbody>
                                </table>

                            @else
                                <div class="well text-center">
                                    ไม่พบเกมที่ค้นหาเลย สะกดถูกป่าว #ผิดชัวร์
                                </div>
                            @endif

                        </div>


                    </div>

                    <div class="panel-footer">* ราคา $ อ้างอิงจาก MiniatureMarket</div>
                </div>
            </div>
        </div>
    </div>



@endsection
