@extends('layouts.app')


@section('title')
    {{ $game->name . " ราคา " . number_format($game->cost_est_min) . "-" . number_format($game->cost_est_max) }}
@endsection
@section('script')
    <script>
        function deletePost(post_id) {

            $('#POST' + post_id + ' .btn').hide();
            axios.post('/post/ajax', {
                        post_id: post_id,
                        mode: 0
                    })
                    .then(function (response) {
                        if (response.data.success) {
                            $('#POST' + post_id).remove();
                        } else {
                            alert('Delete fail' + response.data.message)
                            $('#POST' + post_id + ' .btn').hide();
                        }
                    })
                    .catch(function (error) {
                        alert('Delete fail ' + error);
                        $('#POST' + post_id + ' .btn').hide();
                    });

        }
    </script>
@endsection

@section('navbar')

@endsection
@section('content')

    <div class="container" id="app-game" xmlns:v-on="http://www.w3.org/1999/xhtml">

        @if(preg_match("/^[^a-z]/i",$game->bgg_name))
            <a class="btn btn-primary" href="{{url("/games-alphabet/0-9")}}">
                <span class="visible-xs ">&lt;&lt; 0-9</span>
                <span class="hidden-xs">&lt;&lt; Alphabet 0-9</span>
            </a>
        @else
            <a class="btn btn-primary" href="{{url("/games-alphabet/".strtolower(substr($game->bgg_name,0,1)))}}">
                <span class="visible-xs">&lt;&lt; {{strtoupper(substr($game->bgg_name,0,1))}}</span>
                <span class="hidden-xs">&lt;&lt; Alphabet {{strtoupper(substr($game->bgg_name,0,1))}}</span>
            </a>
        @endif


        <form action="{{url("/games/search")}}" class="col-xs-9 col-sm-6 col-md-4 pull-right">
            <div class="input-group ">
                <input type="text" class="form-control" placeholder="Search for..." name="keyword"/>
                                  <span class="input-group-btn">
                                    <input class="btn btn-default" type="submit" value="Go!"/>
                                  </span>

            </div><!-- /input-group -->
        </form>


        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h3>ราคากลางบอร์ดเกม</h3>
                <h1>{{$game->bgg_name}}

                    @if ($game->cost_est_min > 0)
                        <br/> ราคา {{number_format($game->cost_est_min)}}
                        - {{number_format($game->cost_est_max)}} บาท </h1>
                @endif
            </div>

            <div class="row">


                <div class="col-md-8">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-5">
                                    <a href="https://boardgamegeek.com/{{strtolower($game->bgg_type)}}/{{$game->bgg_id}}/{{$game->bgg_slug}}"
                                       target="_blank">
                                        <img src="{{str_replace("cf.geekdo-images.com","cf.thaigamemarket.com",$game->bgg_image)}}"
                                             class="img-thumbnail"/>
                                    </a>
                                </div>
                                <div class="col-sm-7">
                                    BGG Link: <a
                                            href="https://boardgamegeek.com/{{strtolower($game->bgg_type)}}/{{$game->bgg_id}}/{{$game->bgg_slug}}"
                                            target="_blank">boardgamegeek.com/{{strtolower($game->bgg_type)}}
                                        /{{$game->bgg_id}}/{{$game->bgg_slug}}</a><br/>
                                    BGG Rating: {{$game->bgg_gbrank}}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="">
                        @if (Auth::guest())
                            <a class="btn btn-success"
                               href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"
                               class="brn btn-facebook">ลงประกาศซื้อ (Login with Facebook)</a>
                            <a class="btn btn-danger"
                               href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"
                               class="brn btn-facebook">ลงประกาศขาย (Login with Facebook)</a>
                        @else
                            {{--@{{ message }}--}}

                            {{--<button  class="btn btn-success"--}}
                            {{--class="brn btn-facebook"--}}
                            {{--v-on:click="toggleBuyForm">--}}
                            {{--ลงประกาศซื้อ--}}
                            {{--</button>--}}
                            {{--<button  class="btn btn-danger"--}}
                            {{--v-on:click="toggleSaleForm"--}}
                            {{--class="brn btn-facebook">ลงประกาศขาย--}}
                            {{--</button>--}}
                            <button v-show="!isOpenBuyForm && !isOpenSaleForm" class="btn btn-success"
                                    class="brn btn-facebook"
                                    v-on:click="toggleBuyForm">
                                ลงประกาศซื้อ
                            </button>
                            <button v-show="!isOpenBuyForm && !isOpenSaleForm" class="btn btn-danger"
                                    v-on:click="toggleSaleForm"
                                    class="brn btn-facebook">ลงประกาศขาย
                            </button>
                        @endif
                    </div>


                    <div class="panel panel-default" v-show="isOpenBuyForm || isOpenSaleForm">
                        <div class="panel-body">


                            @if (Auth::guest())
                                {{--<a class="btn btn-success"--}}
                                {{--href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"--}}
                                {{--class="brn btn-facebook">ลงประกาศซื้อ (Login with Facebook)</a>--}}
                                {{--<a class="btn btn-danger"--}}
                                {{--href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"--}}
                                {{--class="brn btn-facebook">ลงประกาศขาย (Login with Facebook)</a>--}}
                            @else
                                {{--@{{ message }}--}}

                                {{--<button v-show="!isOpenBuyForm && !isOpenSaleForm" class="btn btn-success"--}}
                                {{--class="brn btn-facebook"--}}
                                {{--v-on:click="toggleBuyForm">--}}
                                {{--ลงประกาศซื้อ--}}
                                {{--</button>--}}

                                <div v-show="isOpenBuyForm">
                                    <h4>ลงประกาศซื้อ</h4>
                                    <form class="form-horizontal" method="post"
                                          action="{{url('/post/create/'.$game->id.'/buy')}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group ">
                                            <label for="buy_first_hand_enable" class="col-xs-3 col-sm-2 control-label">มือหนึ่ง
                                                <input id="buy_first_hand_enable" type="checkbox" name="enable_1"
                                                       v-model="BuyFormData.is1Hand" value="1"/>
                                            </label>
                                            <div class="col-xs-7 col-sm-5 col-md-5" v-show="BuyFormData.is1Hand">
                                                <div class="input-group">
                                                    <div class="input-group-addon">ราคาไม่เกิน</div>
                                                    <input type="number" class="form-control" name="price_1"
                                                           placeholder="ไม่ระบุ">
                                                    <div class="input-group-addon">บาท</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="buy_second_hand_enable" class="col-xs-3 col-sm-2 control-label">มือสอง
                                                <input id="buy_second_hand_enable" type="checkbox" name="enable_2"
                                                       v-model="BuyFormData.is2Hand" value="1"/>

                                            </label>
                                            <div class="col-xs-7 col-sm-5 col-md-5" v-show="BuyFormData.is2Hand">
                                                <div class="input-group">
                                                    <div class="input-group-addon">ราคาไม่เกิน</div>
                                                    <input type="number" class="form-control" name="price_2"
                                                           placeholder="ไม่ระบุ">
                                                    <div class="input-group-addon">บาท</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Comment
                                            </label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" rows="3" name="comment"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-success">ลงประกาศซื้อ</button>
                                                <button type="submit" class="btn"
                                                        v-on:click.stop.prevent="closeBuyForm">ยกเลิก
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <hr/>
                                </div>



                                <div v-show="isOpenSaleForm">
                                    <h4>ลงประกาศขาย</h4>
                                    <form enctype="multipart/form-data" class="form-horizontal" method="post"
                                          action="{{url('/post/create/'.$game->id.'/sell')}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label for="" class="col-xs-3 col-sm-2 control-label">สภาพ
                                            </label>
                                            <div class="col-xs-3 col-sm-2">
                                                <div class="input-group">
                                                    <label>
                                                        <input type="radio" name="item_hand" checked
                                                               value="1"/>
                                                        มือหนึ่ง
                                                    </label>

                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-sm-2">
                                                <div class="input-group">
                                                    <label>
                                                        <input type="radio" name="item_hand"
                                                               value="2"/>
                                                        มือสอง
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-xs-3 col-sm-2 control-label">ราคาขาย
                                            </label>
                                            <div class="col-xs-9 col-sm-5 col-md-5">
                                                <div class="input-group">
                                                    <div class="input-group-addon">ไม่ต่ำกว่า</div>
                                                    <input type="number" class="form-control" name="price"
                                                           placeholder="ไม่ระบุ">
                                                    <div class="input-group-addon">บาท</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Comment
                                            </label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" rows="3" name="comment"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">รูปภาพประกอบ
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="file" name="file1" class="form-control"/><br/>
                                                <input type="file" name="file2" class="form-control"/><br/>
                                                <input type="file" name="file3" class="form-control"/><br/>
                                                <input type="file" name="file4" class="form-control"/><br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-danger">ลงประกาศขาย</button>
                                                <button type="submit" class="btn"
                                                        v-on:click.stop.prevent="closeSaleForm">ยกเลิก
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <hr/>
                                </div>


                            @endif


                        </div>
                    </div>

                    <hr/>

                    @if(count($buy_posts)>0)
                        @foreach($buy_posts as $post)


                            <div class="panel panel-default" id="POST{{$post->id}}">

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-2 col-md-6">
                                            <div class="row">
                                                <div class="col-xs-10 col-md-3">
                                                    <img src="{{$post->user->profile_url}}" class="img-thumbnail"/>
                                                </div>
                                                <div class="col-xs-10 col-md-9">
                                                    <span style="font-size: 18px"><a target="_blank"
                                                                                     href="https://www.facebook.com/{{$post->user->facebook_id}}">{{$post->user->name}}</a>
                                                    </span><br/>
                                                    {{$post->created_at->toFormattedDateString()}}
                                                    <i>({{$post->created_at->diffForHumans()}})</i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 text-left">
                                            @if($post->enable_1)
                                                <h4>
                                                    <span class="text-success">[ซื้อ]</span> มือ 1
                                                </h4>
                                            @endif
                                            @if($post->enable_2)
                                                <h4>
                                                    <span class="text-success">[ซื้อ]</span> มือ 2
                                                </h4>
                                            @endif
                                        </div>
                                        <div class="col-md-2 text-right">
                                            @if($post->enable_1)
                                                <h4>
                                                    {{$post->price_1>0?" ".number_format($post->price_1)."":"-"}}
                                                </h4>
                                            @endif
                                            @if($post->enable_2)
                                                <h4>
                                                    {{$post->price_2>0?" ".number_format($post->price_2)."":"-"}}
                                                </h4>
                                            @endif
                                        </div>

                                        <div class="col-xs-4 col-md-2 text-right">

                                            @if (!Auth::guest() && Auth::user()->id==$post->user_id)
                                                <a class="btn btn-danger"
                                                   href="#" onclick="deletePost({{$post->id}});return false">ลบ</a>
                                            @else

                                                <a class="btn btn-primary" target="_blank"
                                                   href="https://www.facebook.com/messages/t/{{$post->user->facebook_id}}">ติดต่อ</a>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(count($sell_posts)>0)

                        @foreach($sell_posts as $post)

                            <div class="panel panel-default" id="POST{{$post->id}}">


                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-2 col-md-6">
                                            <div class="row">
                                                <div class="col-xs-10 col-md-3">
                                                    <img src="{{$post->user->profile_url}}" class="img-thumbnail"/>
                                                </div>
                                                <div class="col-xs-10 col-md-9">
                                                    <span style="font-size: 18px"><a target="_blank"
                                                                                     href="https://www.facebook.com/{{$post->user->facebook_id}}">{{$post->user->name}}</a>
                                                    </span><br/>
                                                    {{$post->created_at->toFormattedDateString()}}
                                                    <i>({{$post->created_at->diffForHumans()}})</i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 text-left">
                                            @if($post->enable_1)
                                                <h4>
                                                    <span class="text-danger">[ขาย]</span> มือ 1
                                                </h4>
                                            @endif
                                            @if($post->enable_2)
                                                <h4>
                                                    <span class="text-danger">[ขาย]</span> มือ 2
                                                </h4>
                                            @endif
                                        </div>
                                        <div class="col-md-2 text-right">
                                            @if($post->enable_1)
                                                <h4>
                                                    {{$post->price_1>0?" ".number_format($post->price_1)."":"-"}}
                                                </h4>
                                            @endif
                                            @if($post->enable_2)
                                                <h4>
                                                    {{$post->price_2>0?" ".number_format($post->price_2)." ":"-ุ"}}
                                                </h4>
                                            @endif
                                        </div>

                                        <div class="col-xs-4 col-md-2 text-right">

                                            @if (!Auth::guest() && Auth::user()->id==$post->user_id)
                                                <a class="btn btn-danger"
                                                   href="#" onclick="deletePost({{$post->id}});return false">ลบ</a>
                                            @else

                                                <a class="btn btn-primary" target="_blank"
                                                   href="https://www.facebook.com/messages/t/{{$post->user->facebook_id}}">ติดต่อ</a>
                                            @endif
                                        </div>


                                    </div>


                                    <div class="row">


                                        <div class="col-md-12">

                                            @if($post->comment)
                                                <pre class="h5" style="border: none">{{trim($post->comment)}}</pre>
                                            @endif

                                        </div>


                                    </div>
                                    <div class="row">

                                        @php
                                        $files = [];
                                        $data = json_decode($post->data,true);
                                        if($data && array_key_exists('files',$data)){
                                        $files=$data['files'];
                                        }
                                        @endphp
                                        @if (count($files)>0)
                                            <hr/>
                                        @endif

                                        @foreach($files as $file)
                                            <a href="{{Storage::url($file['path'])}}" target="_blank"
                                               class="col-md-2">
                                                <img src="{{Storage::url($file['thumbnail'])}}"
                                                     class="img-thumbnail img-responsive"/>
                                            </a>
                                        @endforeach
                                    </div>


                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>


                <div class="col-md-4">

                    @if ($game->cost_est_min == 0)

                        <div class="panel panel-default">
                            <div class="panel-body">
                                ยังไม่มีข้อมูลราคา
                            </div>
                        </div>
                    @else
                        @if($game->mnmk_price>0 && $game->mnmk_price<$game->mnmk_retail*0.9)

                            <div class="panel panel-default">
                                <div class="panel-heading"><h4>คำนวนแบบง่าย เอาราคาลดคูณ 40</h4></div>

                                <div class="panel-body">


                                    <div class="table-responsive-disable">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>รายการ</th>
                                                <th class="text-right">USD</th>
                                                <th class="text-right">THB(40)</th>
                                            </tr>
                                            </thead>

                                            <tbody>


                                            <tr>
                                                <td>ราคา <strong>ลด</strong> Miniature Market</td>
                                                <td class="text-right">${{$game->mnmk_price}}</td>
                                                <td class="text-right">
                                                    <strong>{{number_format($game->mnmk_price*40)}}</strong>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>Vat 7%</td>
                                                <td class="text-right"></td>
                                                <td class="text-right">
                                                    <strong>{{number_format($game->mnmk_price*40*0.07)}}</strong></td>

                                            </tr>
                                            <tr>
                                                <td><h3>ราคา</h3></td>
                                                <td class="text-right" colspan="2">
                                                    <h3><strong>{{number_format($game->mnmk_price*40*1.07)}}</strong>
                                                    </h3>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>คำนวนแบบง่าย เอาราคาเต็มคูณ 40</h4></div>

                            <div class="panel-body">


                                <div class="table-responsive-disable">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>รายการ</th>
                                            <th class="text-right">USD</th>
                                            <th class="text-right">THB(40)</th>
                                        </tr>
                                        </thead>

                                        <tbody>


                                        <tr>
                                            <td>ราคา <strong>เต็ม</strong> Miniature Market</td>
                                            <td class="text-right">${{$game->cost_msrp}}</td>
                                            <td class="text-right">
                                                <strong>{{number_format($game->cost_msrp*40)}}</strong>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>vat 7%</td>
                                            <td class="text-right"></td>
                                            <td class="text-right">
                                                <strong>{{number_format($game->cost_msrp*40*0.07)}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><h3>ราคา</h3></td>
                                            <td class="text-right" colspan="2">
                                                <h3><strong>{{number_format($game->cost_msrp*40*1.07)}}</strong></h3>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>คำนวนด้วยวิธีการส่งทางเรือ</h4> <em>(รอประมาณ 2-5 เดือน)</em>
                            </div>

                            <div class="panel-body">


                                <div class="table-responsive-disable">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>รายการ</th>
                                            <th class="text-right">USD</th>
                                            <th class="text-right">THB(35)</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>ราคา Miniature Market</td>
                                            <td class="text-right">${{$game->cost_discount}}</td>
                                            <td class="text-right">
                                                <strong>{{number_format($game->cost_dis_game)}}</strong>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>ค่าส่งจากอเมริกาถึงไทย<br/><em> (กิโลละ 400-450 โดยประมาณ)</em></td>
                                            <td class="text-right"></td>
                                            <td class="text-right">
                                                <strong>{{number_format($game->cost_shipping)}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>ค่าส่งในไทย</td>
                                            <td class="text-right"></td>
                                            <td class="text-right">
                                                <strong>{{number_format($game->cost_thai_post)}}</strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text-right" colspan="3">
                                                <h3 class="pull-right">
                                                    <strong>{{number_format($game->cost_no_tax)}}</strong></h3>
                                                <h3 class="pull-left">ราคาไม่รวมภาษี </h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>ภาษีนำเข้า <br/><em>(คิดจากราคาหน้ากล่อง)</em></td>
                                            <td class="text-right">${{$game->cost_msrp}} <strong> X 20%</strong></td>
                                            <td class="text-right">
                                                <strong>{{number_format($game->cost_custom)}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>ภาษีมูลค่าเพิ่ม <br/><em>(คิดจากราคาหน้ากล่อง)</em></td>
                                            <td class="text-right">${{$game->cost_msrp}} <strong> X 7%</strong></td>
                                            <td class="text-right"><strong>{{number_format($game->cost_vat)}}</strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><h3>ราคารวมภาษี</h3></td>

                                            <td class="text-right" colspan="2">
                                                <h3><strong>{{number_format($game->cost_with_tax)}}</strong></h3>
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    @endif
                </div>
                {{--End of left panel--}}
            </div>
        </div>


        <div class="row">


        </div>


    </div>

@endsection
