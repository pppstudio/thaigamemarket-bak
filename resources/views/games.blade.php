@extends('layouts.app')

@section('title')
    ราคากลางบอร์ดเกม อักษร {{strtoupper($active)}}
@endsection

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">รายชื่อเกม</div>

                    <div class="panel-body">
                        <form action="{{url("/games/search")}}">
                            <div class="input-group col-md-6">
                                <input type="text" class="form-control" placeholder="Search for..." name="keyword"
                                       value="{{$keyword}}"/>
                                  <span class="input-group-btn">
                                    <input class="btn btn-default" type="submit" value="Go!"/>
                                  </span>

                            </div><!-- /input-group -->
                        </form>
                        <ul class="pagination">
                            <li class="{{$active=='0-9'?'active':''}}"><a href="{{url("/games-alphabet/0-9")}}">0-9 </a>
                            </li>
                            @foreach (range('a', 'z') as $char)
                                <li class="{{$active==$char?'active':''}}"><a
                                            href="{{url("/games-alphabet/".$char)}}">{{strtoupper($char)}}</a></li>
                            @endforeach
                        </ul>
                        <div class="table-responsive-disable">

                            @if (count($games)>0)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                            ชื่อ
                                        </th>
                                        <th class="text-right visible-md visible-lg">
                                            ราคา $
                                        </th>
                                        <th class="text-right visible-md visible-lg">
                                            ~ราคา
                                        </th>
                                        <th class="text-right visible-md visible-lg">
                                            ซื้อ/ขาย
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- @var \App\Game $game --}}
                                    @foreach ($games as $game)
                                        @php
                                        $post = null;
                                        $mode = 0;
                                        if($posts){
                                        $post = $posts->where('game_id',$game->id)->last();
                                        if($post){
                                        if($post->post_type == \App\Post::POST_TYPE_BUY){
                                        $mode = 1;
                                        }
                                        if($post->post_type == \App\Post::POST_TYPE_SALE){
                                        $mode = 2;
                                        }
                                        }
                                        }
                                        @endphp
                                        <tr>
                                            <td class="name">
                                                <a class="visible-xs visible-sm big"
                                                   href="{{url("/game/".$game->bgg_id."/".$game->slug)}}">{{$game->bgg_name}}</a>
                                                <a class="visible-md visible-lg"
                                                   href="{{url("/game/".$game->bgg_id."/".$game->slug)}}">{{$game->bgg_name}}</a>

                                                <div class="visible-xs visible-sm">
                                                    <span class="big pull-right">
                                                        @if ($game->cost_est_min>0)
                                                            {{number_format($game->cost_est_min)}}
                                                            - {{number_format($game->cost_est_max)}}

                                                        @else
                                                            -
                                                        @endif
                                                        </span>
                                                <span class="big">
                                                    @if ($game->cost_msrp>0)
                                                        @if($game->mnmk_price>0)
                                                            <s>${{number_format($game->cost_msrp,2)}}</s>
                                                            <strong>${{number_format($game->mnmk_price,2)}}</strong>
                                                        @else
                                                            <strong>${{number_format($game->cost_msrp,2)}}</strong>
                                                        @endif
                                                    @else
                                                        -
                                                    @endif
                                                </span>
                                                    <div class="clearfix"></div>
                                                    <div class="pull-right">

                                                        @if (Auth::guest())
                                                            <a class="btn btn-success"
                                                               href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"
                                                               class="brn btn-facebook">ซื้อ</a>
                                                            <a class="btn btn-danger"
                                                               href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"
                                                               class="brn btn-facebook">ขาย</a>
                                                        @else
                                                            <form_post_buy ex_class="small" def_mode="{{$mode}}"
                                                                           game_id="{{$game->id}}"
                                                                           def_post_id="{{$post?$post->id:''}}"
                                                                           def_buy_init="{{floor($game->cost_est_min/100)*100}}"
                                                                           def_sell_init="{{ceil($game->cost_est_max/100)*100}}"
                                                                           def_buy_value="{{$post&&$post->post_type==\App\Post::POST_TYPE_BUY?$post->price_1:''}}"
                                                                           def_sell_value="{{$post&&$post->post_type==\App\Post::POST_TYPE_SALE?$post->price_1:''}}"
                                                            />

                                                        @endif
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </td>
                                            <td class="text-right visible-md visible-lg">
                                                @if ($game->cost_msrp>0)

                                                    @if($game->mnmk_price>0)
                                                        <s>${{number_format($game->cost_msrp,2)}}</s>
                                                        <strong>{{number_format($game->mnmk_price,2)}}</strong>
                                                    @else
                                                        <strong>{{number_format($game->cost_msrp,2)}}</strong>
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td class="text-right text-right visible-md visible-lg">
                                                @if ($game->cost_est_min>0)
                                                    <strong>  {{number_format($game->cost_est_min)}}
                                                        - {{number_format($game->cost_est_max)}}</strong>
                                                @else
                                                    -
                                                @endif

                                            </td>
                                            <td class="buy_sell visible-md visible-lg">

                                                @if (Auth::guest())
                                                    <a class="btn btn-success"
                                                       href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"
                                                       class="brn btn-facebook">ซื้อ</a>
                                                    <a class="btn btn-danger"
                                                       href="{{ url('/login/facebook')."?redirect=". urlencode(url()->current()) }}"
                                                       class="brn btn-facebook">ขาย</a>
                                                @else
                                                    {{--{{$post}}--}}
                                                    <form_post_buy ex_class="large" def_mode="{{$mode}}"
                                                                   game_id="{{$game->id}}"
                                                                   def_post_id="{{$post?$post->id:''}}"
                                                                   def_buy_init="{{floor($game->cost_est_min/100)*100}}"
                                                                   def_sell_init="{{ceil($game->cost_est_max/100)*100}}"
                                                                   def_buy_value="{{$post&&$post->post_type==\App\Post::POST_TYPE_BUY?$post->price_2:''}}"
                                                                   def_sell_value="{{$post&&$post->post_type==\App\Post::POST_TYPE_SALE?$post->price_2:''}}"
                                                    />
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            @else
                                <div class="well text-center">
                                    ยังไม่พบเกมที่ค้นหา อาจจะเป็นเพราะเรายังไม่ได้โหลดข้อมูลมา<br/> ลองกดปุ่ม
                                    ค้นเพิ่มเติม ด้านล่างดูนะ <br/>
                                    #เด๋วมันจะโหลดมาเพิ่มเอง
                                </div>
                            @endif

                        </div>

                        @if ($keyword)

                            <div class="text-center"><a class="btn btn-primary"
                                                        href="{{url("games/search_more?keyword=".$keyword)}}">

                                    @if (count($games)>0)
                                        ยังไม่เจอเกมที่หา กดค้นเพิ่มเติม ที่นี่
                                    @else
                                        ค้นเพิ่มเติม กดที่นี่
                                    @endif
                                </a></div>
                        @else
                            #บางทีเกมที่ลีสตามอักษรอาจจะมาไม่หมด ถ้าหาไม่เจอให้กด search ด้านบนเลยนะ
                        @endif

                    </div>

                    <div class="panel-footer">* ราคา $ อ้างอิงจาก MiniatureMarket</div>
                </div>
            </div>
        </div>
    </div>



@endsection
