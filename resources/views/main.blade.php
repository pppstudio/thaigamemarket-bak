@extends('layouts.app')

@section('content')
    @yield('menu')
    @yield('body')
@endsection