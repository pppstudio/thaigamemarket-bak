<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App
 * @property number user_id
 */
class Post extends Model
{
    const POST_TYPE_BUY = 10;
    const POST_TYPE_SALE = 20;

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'game_id', 'enable_1', 'enable_2', 'price_1', 'price_2', 'comment'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function game()
    {
        return $this->belongsTo('App\Game');
    }
}
