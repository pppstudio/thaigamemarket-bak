<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


/**
 * Class Game
 * @package App
 * @property string bgg_id
 * @property string bgg_slug
 * @property string bgg_name
 * @property float bgg_gbrank
 * @property float bgg_lcrank
 * @property float bgg_lcvote
 * @property string bgg_type
 * @property string bgg_image
 * @property integer bgg_year
 * @property integer bgg_own
 * @property float bgg_esw
 * @property float bgg_esh
 * @property float bgg_esl
 * @property float bgg_eswg
 * @property object bgg_last_update
 * @property integer mnmk_id
 * @property string mnmk_name
 * @property string mnmk_image
 * @property integer mnmk_stock
 * @property object mnmk_last_update
 * @property float mnmk_price
 * @property float mnmk_retail
 * @property string mnmk_url
 * @property string mnmk_code
 *
 *
 */
class Game extends Model
{

    protected $fillable = [
        'bgg_id',
        'bgg_slug',
        'bgg_name',
        'bgg_gbrank',
        'bgg_lcrank',
        'bgg_lcvote',
        'bgg_type',
        'bgg_image',
        'bgg_year',
        'bgg_own',
        'bgg_esw',
        'bgg_esh',
        'bgg_esl',
        'bgg_eswg',
        'bgg_last_update',
        'mnmk_id',
        'mnmk_name',
        'mnmk_image',
        'mnmk_stock',
        'mnmk_last_update',
        'mnmk_price',
        'mnmk_retail',
        'mnmk_url',
        'mnmk_code'
    ];


    static function cacheSearchByAlphabetQuery($char, $minutes = 60)
    {
        $value = null;
        if ($char == '0-9') {
            $value = Cache::remember('game::cacheSearchByAlphabetQuery(' . $char . ')', $minutes, function () {
                return Game::Query()->where('bgg_name', 'REGEXP', '^[0-9]')->orderBy('bgg_name')->get();
            });
        } else {
            $value = Cache::remember('game::cacheSearchByAlphabetQuery(' . $char . ')', $minutes, function () use ($char) {
                return Game::Query()->where('bgg_name', 'like', $char . '%')->orWhere('bgg_name', 'like', 'the ' . $char . '%')->orderBy('bgg_name')->get();
            });
        }
        return $value;
    }


    static function cacheSearchByKeywordQuery($keyword, $minutes = 60)
    {
        $value = Cache::remember('game::cacheSearchByKeywordQuery(' . strtolower($keyword) . ')', $minutes, function () use ($keyword) {
            return Game::Query()->where('bgg_name', 'like', '%' . $keyword . '%')->orderBy('bgg_name')->get();
        });
        return $value;
    }

    public function getNameAttribute()
    {
        if ($this->bgg_name) {
            return $this->bgg_name;
        }
        return $this->bgg_name;
    }

    public function getSlugAttribute()
    {
        if ($this->bgg_slug) {
            return $this->bgg_slug;
        }
        return strtolower(preg_replace("/[^0-9a-z]/i", "", $this->bgg_name));
    }


    public function getCostDiscountAttribute()
    {
        if ($this->mnmk_price > 0) {
            return $this->mnmk_price;
        }
        if ($this->mnmk_retail > 0) {
            return $this->mnmk_retail;
        }
        if ($this->dist_retail > 0 && $this->dist_retail > $this->dist_price) {
            return $this->dist_retail;
        }

        return $this->dist_price * 100 / 65;
    }

    public function getCostMsrpAttribute()
    {

        if ($this->mnmk_retail > 0) {
            return $this->mnmk_retail;
        }
        if ($this->dist_retail > 0 && $this->dist_retail > $this->dist_price) {
            return $this->dist_retail;
        }

        if ($this->dist_price > 0) {
            return $this->dist_price * 100 / 65;
        }
        return $this->mnmk_price;


    }


    public function getCostEstTotalAttribute()
    {

        return $this->cost_no_tax;
    }

    public function getCostEstMinAttribute()
    {
        return min($this->cost_simple_full_40, $this->cost_no_tax);
    }

    public function getCostEstMaxAttribute()
    {
        return max($this->cost_simple_full_40, $this->cost_no_tax);
    }


    public function getCostSimpleFull40Attribute()
    {

        if ($this->cost_msrp == 0) {
            return $this->cost_discount * 40 * 1.07;
        }
        return $this->cost_msrp * 40 * 1.07;
    }


    public function getCostDisGameAttribute()
    {
        $rate_thb = 35;
        return $this->cost_discount * $rate_thb;
    }


    public function getCostShippingAttribute()
    {
        if ($this->bgg_eswg == 0) {
            return 400;
        }
        if ($this->bgg_eswg < 1) {

            return $this->bgg_eswg * 500;
        }

        if ($this->bgg_eswg > 1 && $this->bgg_eswg < 1.5) {
            return $this->bgg_eswg * (400 + 100 * (1.5 - $this->bgg_eswg) / 0.5);
        }

        return $this->bgg_eswg * 400;

    }

    public function getCostThaiPostAttribute()
    {
        if ($this->bgg_eswg == 0) {
            return 150;
        }
        return ($this->bgg_eswg > 2 ? 200 : (($this->bgg_eswg > 1 ? 150 : (100))));

    }

    public function getCostNoTaxAttribute()
    {
        return $this->cost_dis_game + $this->cost_shipping + $this->cost_thai_post;
    }


    static function SearchBggByKeyword($keyword)
    {
        $url = "search?query=" . str_replace(" ", "+", $keyword) . "&type=boardgame,boardgameexpansion&stats=1&versions=1";
        //echo $url;

        $itemArray = self::loadBGGAPI($url, 60 * 24 * 7);

        if (!array_key_exists('item', $itemArray)) {
            return [];
        }

        $loadCollecter = array();
        //var_dump($itemArray);
        $thingItems = array();

        if (!$itemArray['item'][0]) {
            $thingItems[] = $itemArray['item'];
        } else {
            $thingItems = $itemArray['item'];
        }

        if ($thingItems) foreach ($thingItems as $item) {
            $type = $item['@attributes']['type'];
            $thing_id = $item['@attributes']['id'];
            $loadCollecter[$type][] = $thing_id;
        }

        $matchObjects = array();
        $count_match = 0;
        $count_unmatch = 0;
        foreach ($loadCollecter as $type => $list) {
            $url = "thing?id=" . implode(",", $list) . "&type=" . $type;
            $boardgameArray = self::loadBGGAPI($url, 60 * 24 * 7 * 10);
            if (!$boardgameArray['item']) continue;
//            var_dump($boardgameArray);

            $thingItems = array();
            if (!$boardgameArray['item'][0]) {
                $thingItems[] = $boardgameArray['item'];
            } else {
                $thingItems = $boardgameArray['item'];
            }
            foreach ($thingItems as $item) {
                if (!array_key_exists('thumbnail', $item)) {
//                    var_dump($item);
                }
                $row =
                    [
                        'id' => $item['@attributes']['id'],
                        'type' => $item['@attributes']['type'],
                        'name' => "",
                        'thumbnail' => "",
                        'image' => "",
                        'year' => $item['yearpublished']['@attributes']['value'],
                        //'names' => $item['name'],
                    ];

                if (array_key_exists('thumbnail', $item)) {
                    $row['thumbnail'] = $item['thumbnail'];
                }
                if (array_key_exists('thumbnail', $item)) {
                    $row['image'] = $item['image'];
                }

                if (array_key_exists('@attributes', $item['name']) && $item['name']['@attributes']['value'] != '') {
                    $row['name'] = $item['name']['@attributes']['value'];
                } else {
                    $row['name'] = $item['name'][0]['@attributes']['value'];
                }

                foreach ($item['link'] as $link) {
                    $row[$link['@attributes']['type']][] = $link['@attributes']['value'];
                }


                $matchObjects[$row['id']] = $row;

                if (strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $keyword)) == strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $row['name']))) {
                    $count_match++;
                    $match_id = $row['id'];
                }

                $count_unmatch++;
                $unmatch_id = $row['id'];
                //break;
            }
        }

//		var_dump($matchObjects);
        return $matchObjects;
    }


    static function loadBGGAPI($url, $minutes = 60)
    {
        $key = 'load_bgg::' . strtolower($url);
        $xml_result = Cache::remember($key, $minutes, function () use ($url) {
            $bgg_url = 'https://www.boardgamegeek.com/xmlapi2/' . $url;
            $client = new \GuzzleHttp\Client();
            $res = $client->get($bgg_url);
            $data = $res->getBody();
//            echo $data;
            return "" . $data . "";
        });

        if (strlen($xml_result) == 0) {
            Cache::forget($key);
        }

        $xml = simplexml_load_string($xml_result);
        $json = json_encode($xml);
        $retArray = json_decode($json, TRUE);
        $retArray['raw'] = $xml_result;

        return $retArray;
    }

    static function createByBGGAPI($id)
    {
        $data = self::LoadBGGPropertiesAPI($id, 'boardgame,boardgameexpansion');

//        var_dump($data);
        /*
         *   'id' => string '26713' (length=5)
          'type' => string 'boardgame' (length=9)
          'thumbnail' => string '//cf.geekdo-images.com/images/pic163426_t.jpg' (length=45)
          'image' => string '//cf.geekdo-images.com/images/pic163426.jpg' (length=43)
          'description' => string 'Pseudo-Coup is another Sudoku-based game. Probably the most beautiful one made. The acrylic nine by nine grid includes laser-cut pieces in nine colors. This allows standard Sudoku to be played with colors instead of numbers (much more pleasing to the eye), and also includes rules for games with up to nine players based on square grids, triangular grids, and more.&#10;&#10;' (length=375)
          'year' => string '2005' (length=4)
          'owned' => string '0' (length=1)
          'minplayers' => string '2' (length=1)
          'maxplayers' => string '9' (length=1)
          'playingtime' => string '30' (length=2)
          'minage' => string '12' (length=2)
          'average' => string '0' (length=1)
          'bayesaverage' => string '0' (length=1)
          'usersrated' => string '0' (length=1)
          'name' => string 'Pseudo-Coup' (length=11)
         */

        $game = new Game();
        $game->bgg_id = $data['id'];
        $game->bgg_gbrank = $data['bayesaverage'];
        $game->bgg_lcrank = $data['average'];
        $game->bgg_lcvote = $data['usersrated'];
        $game->bgg_year = $data['year'];
        $game->bgg_own = $data['owned'];
        $game->bgg_name = $data['name'];
        $game->bgg_slug = strtolower($data['name']);
        $game->bgg_image = $data['image'];
        $game->bgg_type = $data['type'];

        $game->bgg_esw = $data['esw'];
        $game->bgg_esh = $data['esh'];
        $game->bgg_esl = $data['esl'];
        $game->bgg_eswg = $data['eswg'];
        $game->bgg_last_update = Carbon::now();

        $product = DB::table('product')
            ->where('bgg_id', $data['id'])
            ->where('mnmk_id','>',0)
            ->get();

        if($product->count()>0){
            $game->mnmk_id = $product[0]->mnmk_id;
            $game->mnmk_name = $product[0]->mnmk_name;
            $game->mnmk_image = $product[0]->mnmk_image;
            $game->mnmk_retail = $product[0]->mnmk_retail;
            $game->mnmk_price = $product[0]->mnmk_price;
            $game->mnmk_stock = $product[0]->mnmk_stock;
            $game->mnmk_code = $product[0]->mnmk_code;
            $game->mnmk_last_update = $product[0]->mnmk_last_update;
            $game->mnmk_url = $product[0]->mnmk_url;
        }else {
            $game->mnmk_id = 0;
            $game->mnmk_name = '';
            $game->mnmk_image = '';
            $game->mnmk_retail = 0;
            $game->mnmk_price = 0;
            $game->mnmk_stock = 0;
            $game->mnmk_code = '';
            $game->mnmk_last_update = '2000-01-01';
            $game->mnmk_url = '';
        }

        $game->est_price_thb = 0;
        $game->dist_price = 0;
        $game->dist_retail = 0;
        $game->dist_id = 0;

        $game->save();

        return $game;

    }

    static function LoadBGGPropertiesAPI($id, $type)
    {

        $url = "thing?id=" . $id . "&type=" . $type . "&versions=1&stats=1";
        //echo $url;
        $boardgameArray = self::loadBGGAPI($url, 60 * 24 * 7);


        if ($boardgameArray === 404) {
            //echo 404;
            return 404;//exit();
        }


        $item = $boardgameArray['item'];

        //echo var_dump($item['statistics']['ratings']);

        $data = self::ExtractBGGProperties($item);


        return $data;
    }


    static function ExtractBGGProperties($item)
    {
        $data = [
            'id' => $item['@attributes']['id'],
            'type' => $item['@attributes']['type'],
            //'name' => $item['name'][0]['@attributes']['value'],
            'thumbnail' => $item['thumbnail'],
            'image' => $item['image'],
            'description' => $item['description'],
            'year' => $item['yearpublished']['@attributes']['value'],
            'owned' => $item['statistics']['ratings']['owned']['@attributes']['value'],
            'minplayers' => $item['minplayers']['@attributes']['value'],
            'maxplayers' => $item['maxplayers']['@attributes']['value'],
            'playingtime' => $item['playingtime']['@attributes']['value'],
            'minage' => $item['minage']['@attributes']['value'],
            'average' => $item['statistics']['ratings']['average']['@attributes']['value'],
            'bayesaverage' => $item['statistics']['ratings']['bayesaverage']['@attributes']['value'],
            'usersrated' => $item['statistics']['ratings']['usersrated']['@attributes']['value'],
            'esw' => 0, 'esh' => 0, 'esl' => 0, 'eswg' => 0
        ];

        if ($item['name']['@attributes']['value'] != '') {
            $data['name'] = $item['name']['@attributes']['value'];
        } else {
            $data['name'] = $item['name'][0]['@attributes']['value'];
        }

//        var_dump($item);
        $last_dimension_p = -1;
        $last_weight_p = -1;


        $versionItems = array();
        if (!is_array($item['versions']['item'])) {
            $versionItems[] = $item['versions']['item'];
        } else {
            $versionItems = $item['versions']['item'];
        }


        foreach ($versionItems as $version) {
            if (!is_array($version)) {
                $version = ['image' => $version];
            }

            $priority = 0;
            if (is_array($version) && array_key_exists('link', $version)) foreach ($version['link'] as $link) {
                if ($link['@attributes']['type'] == 'language' && $link['@attributes']['value'] == 'English') {
                    $priority = 1;
                }
            }
            $width = array_key_exists('width', $version) ? $version['width']['@attributes']['value'] : 0;
            $length = array_key_exists('length', $version) ? $version['length']['@attributes']['value'] : 0;
            $depth = array_key_exists('depth', $version) ? $version['depth']['@attributes']['value'] : 0;
            $weight = array_key_exists('weight', $version) ? $version['weight']['@attributes']['value'] : 0;

            if ((!array_key_exists('esw', $data) || $priority > $last_dimension_p) && $width != '' && $length != '' && $depth != '') {
                $data['esw'] = $width;
                $data['esl'] = $length;
                $data['esh'] = $depth;
                $last_dimension_p = $priority;
            }
            if ((!array_key_exists('eswg', $data) || $priority > $last_weight_p) && $weight != '') {
                $data['eswg'] = $weight;
                $last_weight_p = $priority;
            }
        }


//        foreach ($item['link'] as $link) {
//            if ($link['@attributes']['type'] == 'boardgamepublisher') {
//                $data['publisher'][] = $link['@attributes']['value'];
//            }
//            if ($link['@attributes']['type'] == 'boardgamecategory') {
//                $data['category'][] = $link['@attributes']['id'];
//                $catObj = MD_BggCategory::BuildByBggID($link['@attributes']['id']);
//                if ($catObj == NULL) {
//                    $catObj = new MD_BggCategory();
//                    $catObj->bgg_id = $link['@attributes']['id'];
//                    $catObj->name_en = $link['@attributes']['value'];
//                    $catObj->Update();
//                }
//            }
//            if ($link['@attributes']['type'] == 'boardgamemechanic') {
//                $data['mechanic'][] = $link['@attributes']['id'];
//                $catObj = MD_BggMechanic::BuildByBggID($link['@attributes']['id']);
//                if ($catObj == NULL) {
//                    $catObj = new MD_BggMechanic();
//                    $catObj->bgg_id = $link['@attributes']['id'];
//                    $catObj->name_en = $link['@attributes']['value'];
//                    $catObj->slug = $slug;
//                    $catObj->Update();
//                }
//            }
//
//
//        }
        return $data;
    }

}

