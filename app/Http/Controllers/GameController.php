<?php

namespace App\Http\Controllers;

use App\Game;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class GameController extends Controller
{

    public function getGameId($id)
    {

        $game = Cache::remember('game::cacheSearchIdQuery(' . $id . ')', $minutes = 60, function () use ($id) {
            $game = Game::Query()->where('id', $id)->first();
//            Cache::remember('game::cacheSearchBggQuery(' . $$game->bgg_id . ')', $minutes = 60, function () use ($game) {
//                return $game;
//            });
            return $game;
        });

        Cache::remember('game::cacheSearchBggQuery(' . $game->bgg_id . ')', $minutes = 60, function () use ($game) {
            return $game;
        });

        return $game;
    }

    public function getGameBgg($bgg_id)
    {
        $game = Cache::remember('game::cacheSearchBggQuery(' . $bgg_id . ')', $minutes = 60, function () use ($bgg_id) {
            $game = Game::Query()->where('bgg_id', $bgg_id)->first();
//            Cache::remember('game::cacheSearchIdQuery(' . $$game->id . ')', $minutes = 60, function () use ($game) {
//                return $game;
//            });
            return $game;
        });


        if ($game) {
            Cache::remember('game::cacheSearchIdQuery(' . $game->id . ')', $minutes = 60, function () use ($game) {
                return $game;
            });
        }

        if (!$game) {
            $game = Game::createByBGGAPI($bgg_id);
//            exit();
        }


        return $game;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function alphabet($char)
    {


        $data = [];

        if ($char != null) {
            $data['games'] = Game::cacheSearchByAlphabetQuery($char, $timeout_minute = 60);
        } else {
            $data['games'] = [];
        }

        $data['active'] = $char;
        $data['keyword'] = '';

        if (!Auth::guest()) {
            $data['posts'] = Post::Query()->where('user_id', Auth::user()->id)->get();
        } else {
            $data['posts'] = null;
        }


        return view('games', $data);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $keyword = $request->get('keyword');
        if ($keyword == null) {
            return view("welcome");
        }


        $data = [];

        $data['games'] = Game::cacheSearchByKeywordQuery($keyword, $timeout_minute = 60);

        $data['active'] = '';
        $data['keyword'] = $keyword;

        if (!Auth::guest()) {
            $data['posts'] = Post::Query()->where('user_id', Auth::user()->id)->get();
        } else {
            $data['posts'] = null;
        }


        return view('games', $data);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function search_more(Request $request)
    {

        $keyword = $request->get('keyword');
        if ($keyword == null) {
            return view("welcome");
        }

        $result = Game::SearchBggByKeyword($keyword);
//        return $result;


        $data = [];

        $data['games'] = $result;

        $data['active'] = '';
        $data['keyword'] = $keyword;

        if (!Auth::guest()) {
            $data['posts'] = Post::Query()->where('user_id', Auth::user()->id)->get();
        } else {
            $data['posts'] = null;
        }


        return view('games_bgg', $data);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function game($bgg_id, $bgg_slug = null)
    {
        if ($bgg_id == null) {
            return 404;
        }
        Carbon::setLocale('th');

        $rate_thb = 35;
        $data = [];

        $game = $this->getGameBgg($bgg_id);


        $data['buy_posts'] = Post::Query()->where('post_type', Post::POST_TYPE_BUY)->where('game_id', $game->id)->with('user')->get();
        $data['sell_posts'] = Post::Query()->where('post_type', Post::POST_TYPE_SALE)->where('game_id', $game->id)->get();


        $data['game'] = $game;


        $data['game']['cost_custom'] = $game->cost_msrp * 20 / 100 * $rate_thb;
        $data['game']['cost_vat'] = $game->cost_msrp * 7 / 100 * $rate_thb;

        $data['game']['cost_with_tax'] = $data['game']['cost_no_tax'] + $data['game']['cost_custom'] + $data['game']['cost_vat'];


        return view('game', $data);
    }


    public function post_ajax(Request $request)
    {
        $post_id = $request->get('post_id');
        $game_id = $request->get('game_id');
        $mode = $request->get('mode');

        switch ($mode + 0) {
            case 0:
                if ($post_id) {
                    /* @var Post $post */
                    $post = Post::Query()->where('id', $post_id)->first();
                    if ($post && $post->user_id != Auth::user()->id) {
                        return ['success' => false, 'message' => 'Permission denied', 'post_id' => $post_id];
                    }
                    $post->delete();
                    return ['success' => true, 'post_id' => 0, 'mode' => $mode];

                }
                break;
            case 1:
                $buy_value = $request->get('buy_value');
                $post = null;
                if ($post_id) {
                    /* @var Post $post */
                    $post = Post::Query()->where('id', $post_id)->first();
                }
                if (!$post) {
                    $post = new Post();
                }
                $post->post_type = Post::POST_TYPE_BUY;

                $post->game_id = $game_id;
                $post->user_id = Auth::user()->id;

                $post->enable_1 = 1;
                $post->enable_2 = 1;

                $post->price_1 = $buy_value + 0;
                $post->price_2 = $buy_value + 0;

                $post->save();
                return ['success' => true, 'post_id' => $post->id, 'mode' => $mode];

            case 2:
                $sell_value = $request->get('sell_value');
                $post = null;
                if ($post_id) {
                    /* @var Post $post */
                    $post = Post::Query()->where('id', $post_id)->first();
                }
                if (!$post) {
                    $post = new Post();
                }
                $post->post_type = Post::POST_TYPE_SALE;
                $post->game_id = $game_id;
                $post->user_id = Auth::user()->id;

                $post->enable_1 = false;
                $post->enable_2 = true;
                $post->price_1 = 0;
                $post->price_2 = $sell_value + 0;

                $post->save();

                return ['success' => true, 'post_id' => $post->id, 'mode' => $mode];
        }


        return ['success' => true, 'post_id' => 0, 'mode' => 0];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function post_buy(Request $request, $game_id)
    {
        if ($game_id == null) {
            return 404;
        }


        $game = $this->getGameId($game_id);


        $post = new Post();
        $post->post_type = Post::POST_TYPE_BUY;
        $post->fill($request->all());
        $post->enable_1 += 0;
        $post->enable_2 += 0;

        $post->price_1 += 0;
        $post->price_2 += 0;

        $post->game_id = $game_id;
        $post->user_id = Auth::user()->id;

        if ($post->enable_1 + $post->enable_2 == 0) {
            // Do nothing if not enable
        } else {
            $post->save();
        }

        return redirect('/game/' . $game->bgg_id . '/' . $game->bgg_slug);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function post_sell(Request $request, $game_id)
    {
        if ($game_id == null) {
            return 404;
        }

        $game = $this->getGameId($game_id);

        $item_hand = $request->get('item_hand');

        $post = new Post();
        $post->post_type = Post::POST_TYPE_SALE;
        $post->fill($request->all());
        $post->game_id = $game_id;
        $post->user_id = Auth::user()->id;
        if ($item_hand == 1) {
            $post->enable_1 = true;
            $post->enable_2 = false;
            $post->price_1 = $request->get('price') + 0;
            $post->price_2 = 0;
        }
        if ($item_hand == 2) {
            $post->enable_1 = false;
            $post->enable_2 = true;
            $post->price_1 = 0;
            $post->price_2 = $request->get('price') + 0;
        }

        $files = ['file1', 'file2', 'file3', 'file4'];
        $filePath = [];
        foreach ($files as $file) {
            if ($request->hasFile($file)) {
                $fileObj = $request->file($file);
                $path = $fileObj->store('public/upload/posts');

                $thumbnailPath = "public/upload/posts/" . basename($path) . "-image(300x200-crop-grayscale).jpg";
                $thumbnailRealPath = storage_path("app/" . $thumbnailPath);

                Image::make($fileObj->getRealPath())->resize(320, 240)->save($thumbnailRealPath);

                if (config('filesystems.default') == 's3') {
                    $thumbnailPath = Storage::putFile("public/upload/posts/thumbnail", new File($thumbnailRealPath));
                }

                $filePath[] = [
                    'driver' => config('filesystems.default'),
                    'path' => $path,
                    'thumbnail' => $thumbnailPath
                ];
            }
        }

        $post->data = json_encode(["files" => $filePath]);

        $post->save();

        return redirect('/game/' . $game->bgg_id . '/' . $game->bgg_slug);

    }
}
