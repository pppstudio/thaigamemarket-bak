<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function redirectToProvider(Request $request)
    {

        session(['redirect' => $request->get('redirect')]);
        return Socialite::driver('facebook')->scopes(['public_profile', 'email'])->redirect();
    }


    public function handleProviderCallback(Request $request)
    {
        try {
            $fbuser = Socialite::driver('facebook')->user();
        } catch (InvalidStateException $e) {
            return redirect("/login/facebook");
        }
//
//
//        // OAuth Two Providers
//        $token = $fbuser->token;
//        $refreshToken = $fbuser->refreshToken; // not always provided
//        $expiresIn = $fbuser->expiresIn;
//
//// OAuth One Providers
//        $token = $fbuser->token;
//        $tokenSecret = $fbuser->tokenSecret;

// All Providers
        $facebook_id = $fbuser->getId();
//        $name = $fbuser->getNickname();
        $name = $fbuser->getName();

        $email = $fbuser->getEmail();

        $profile_url = $fbuser->getAvatar();

        $user = User::Query()->where('email', $email)->first();
        if ($user == null) {
            $user = new User();
            $user->fill(['email' => $email, 'password' => bcrypt(random_bytes(8))]);
        }

        $user->facebook_id = $facebook_id;
        $user->name = $name;
        $user->profile_url = $profile_url;
        $user->save();


        Auth::loginUsingId($user->id, true);

        $redirect = session('redirect');
        if ($redirect) {
            return redirect($redirect);
        }
        return redirect('/');
        // $user->token;
    }
}
