<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [];


        $buy_posts = Cache::remember('post:buy:last', $minutes = 60, function () {
            $posts = Post::Query()->where('post_type', Post::POST_TYPE_BUY)->with('user')->with('game')->orderBy('created_at', 'desc')->take(5)->get();
            return $posts;
        });
        $sell_posts = Cache::remember('post:sell:last', $minutes = 60, function () {
            $posts = Post::Query()->where('post_type', Post::POST_TYPE_SALE)->with('user')->with('game')->orderBy('created_at', 'desc')->take(5)->get();
            return $posts;
        });

        $data['buy_posts'] = $buy_posts;
        $data['sell_posts'] = $sell_posts;
        return view('welcome', $data);
    }
}
