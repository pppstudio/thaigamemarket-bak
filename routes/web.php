<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/games-alphabet/{char}', 'GameController@alphabet');
Route::get('/games/search', 'GameController@search');
Route::get('/games/search_more', 'GameController@search_more');

Route::get('/game/{bgg_id}', 'GameController@game');
Route::get('/game/{bgg_id}/{bgg_slug}', 'GameController@game');


Route::get('/login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('/login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::group(['middleware' => ['auth']], function () {
    Route::post('/post/create/{game_id}/buy', 'GameController@post_buy');
    Route::post('/post/create/{game_id}/sell', 'GameController@post_sell');

    Route::post('/post/ajax', 'GameController@post_ajax');

});


//Route::get('/thumbnail/{file}', function()
//{
//
//    $img = Image::make('foo.jpg')->resize(300, 200);
//
//    return $img->response('jpg');
//});

