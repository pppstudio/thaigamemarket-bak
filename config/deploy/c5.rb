set :user, "bgputon"
set :deploy_to, "/home/bgputon/public_html/apps/thaigamemarket.com"

server "boardgameputon.com",:web,:app,:db

before "deploy:create_symlink" , "symlink_fusion_preconfig" 
after "deploy:create_symlink" , "symlink_fusion_postconfig" 

ssh_options[:keys] = %w('~/.ssh/id_rsa')