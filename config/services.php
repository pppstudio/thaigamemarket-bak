<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY','AKIAJB7UMAMPEZ5LVQ5A'),
        'secret' => env('SES_SECRET','Amfc6GZK+pjPExfkUwXreYp4QcT8Myn18O0snkoBwC27'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '1924261337795897',
        'client_secret' => '9699bf80012ccf389e1aee4f191a56f5',
        'redirect' => 'http://' . $_SERVER['SERVER_NAME'] . '/login/facebook/callback',
    ],
    'cdn' => [
        'domain' => env('CDN_DOMAIN','d15swpg7fqnnh2.cloudfront.net'),
    ],

];
